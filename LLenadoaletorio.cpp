#include <iostream>
#include <cstdlib> // Para la funci�n rand()
#include <ctime>   // Para la funci�n time()
#include <limits>  // Para usar los valores m�ximos de int
using namespace std;

class ejemplo {
	int iprivado, isegundo;
public:
	ejemplo() {
		this->iprivado = 0;
		this->isegundo = 0;
	}
	int get_iprivado() {
		return iprivado;
	}
	int get_isegundo() {
		return isegundo;
	}
	void set_iprivado(int valor) {
		if (valor > 0)
			iprivado = valor;
	}
	void set_isegundo(int valor) {
		if (valor > 0)
			isegundo = valor;
	}
	void set_ejemplo(int valor, int valor2) {
		if (valor > 0 && valor2 > 0) {
			iprivado = valor;
			isegundo = valor2;
		}
	}
	void imprimir() {
		cout << "iprivado= " << iprivado << ", isegundo= " << isegundo << endl;
	}
	void llenadoAleatorio(int min, int max) {
		iprivado = min + rand() % (max - min + 1);
		isegundo = min + rand() % (max - min + 1);
	}
	int mayor() {
		return (iprivado > isegundo) ? iprivado : isegundo;
	}
	~ejemplo() {
		iprivado = -1;
		isegundo = -1;
	
	}
};

void imprimirArreglo(ejemplo arrObjeto[], int tam) {
	for (int i = 0; i < tam; ++i) {
		cout << "Objeto " << i + 1 << ": ";
		arrObjeto[i].imprimir();
	}
}

void llenarArregloAleatoriamente(ejemplo arrObjeto[], int tam, int min, int max) {
	for (int i = 0; i < tam; ++i) {
		arrObjeto[i].llenadoAleatorio(min, max);
	}
}

void imprimirMayores(ejemplo arrObjeto[], int tam) {
	int mayorIprivado = numeric_limits<int>::min();
	int mayorIsegundo = numeric_limits<int>::min();
	for (int i = 0; i < tam; ++i) {
		mayorIprivado = max(mayorIprivado, arrObjeto[i].get_iprivado());
		mayorIsegundo = max(mayorIsegundo, arrObjeto[i].get_isegundo());
	}
	cout << "Mayor valor de iprivado entre todos los objetos: " << mayorIprivado << endl;
	cout << "Mayor valor de isegundo entre todos los objetos: " << mayorIsegundo << endl;
}

int main(int argc, char *argv[]) {
	const int TAM = 20; // Cambiar el tama�o a 20
	ejemplo arrObjeto[TAM];
	
	// Semilla para la funci�n rand()
	srand(time(NULL));
	
	// Llenar los objetos con valores aleatorios
	llenarArregloAleatoriamente(arrObjeto, TAM, 1, 1000);
	
	// Imprimir los valores de los objetos
	imprimirArreglo(arrObjeto, TAM);
	
	// Encontrar y imprimir los mayores valores de iprivado e isegundo
	imprimirMayores(arrObjeto, TAM);
	
	return 0;
}

